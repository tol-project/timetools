/////////////////////////////////////////////////////////////////////////////
// Checks InvChFlow function that transforms the dating of a flow series to
// a finest harmonic dating. In this case transforms a weekly flow time 
// series in a daily flow time series which weekly sum is exactly the same
// than original.
/////////////////////////////////////////////////////////////////////////////
#Require TimeTools;

/////////////////////////////////////////////////////////////////////////////
//Carga las series disponibles para elchequeo de la inversi�n suavizada de 
//fechado
Set If(!ObjectExist("Set","Series"),Include("loadAndClassify.tol"));
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//Toma una submuestra de tama�o sampleSize o la muestra total si se excede
//el tama�o muestral total
//par�metros de selecci�n de la submuestra aleatoria
Real If(ObjectExist("Real","sampleSize"),sampleSize,sampleSize=1/0);
Real If(ObjectExist("Real","randomSeed"),randomSeed,randomSeed=765);
Real PutRandomSeed(randomSeed);
Set subSample = If(sampleSize>=Card(Series),Series,
  ExtractByIndex(Series,
  MatSet(Sub(RandPermutation(1,Card(Series)),1,1,1,sampleSize))[1]));
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//Se desea suavizar todas las series escogidas en diario de forma que su 
//agregaci�n en el fechado original con el estad�stico suma ns devuelva 
//la serie original. Tambi�n se puede prbar cn la media (AvrS).
WriteLn("Building smooth series ...");
Real t0 := Copy(Time);
Text If(ObjectExist("Text","groupStat"),groupStat,groupStat="SumS");
@TimeSet trg = [[Daily]];
Set dailySeries = { TimeTools::InvChFlow.Set(subSample, trg, groupStat,2) }; 
Real time.InvChFlow = (Copy(Time)-t0) / Card(dailySeries);
WriteLn("time.InvChFlow: "<<time.InvChFlow);
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//Construye las series agregadas a partir de las suavizadas para comprobar 
//que efectivamente coinciden con la original
WriteLn("Building aggregated series for checking ...");
Real t0 := Copy(Time);
Set agg = For(1,Card(dailySeries), Serie(Real k)
{
  Text expr = Name(dailySeries[k])+"_agg="+
    "DatCh(dailySeries[k],Dating(subSample[k]),"+groupStat+")";
  Eval(expr)
});
Real time.agg = (Copy(Time)-t0) / Card(dailySeries);
WriteLn("time.agg: "<<time.agg);
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//Construye las series de escalones para la cmparaci�n visual contra las
//suavizadas
WriteLn("Building step spline for comparissons ...");
Real t0 := Copy(Time);
Set steps = EvalSet(subSample, Serie(Serie s)
{
  Serie weight = If(groupStat == "AvrS", CalVar($trg,Dating(s)), 1);
  Serie acum = DifEq(B/(1-B), s*weight, 0);
  Serie acumD = StdLib::Interpolate::Scalar::invCh(acum, $trg, "linear", 1);
  Eval(Name(s)+"_step=(F-1):acumD")
}); // escalones
Real time.step = (Copy(Time)-t0) / Card(dailySeries);
WriteLn("time.step: "<<time.step);
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//Recoloca todas las series para facilitar su visualizaci�n en un conjunto
//de conjuntos con 4 series cada uno
// * La serie original Sxxxxx
// * La serie agregada a partir de la suavizada Sxxxxx_agg
// * La serie diarizada en escalones Sxxxxx_step
// * La serie diarizada Sxxxxx
Set checkSeries = {
  Set aux = Traspose([[subSample,agg,steps,dailySeries]]);
  EvalSet(aux,Set(Set S)
  {
    Eval(Name(S[1])+"=S")
  })
};
Real SetIndexByName(checkSeries);
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//Comprueba que las series suavizadas cumplen las condiciones
// * La serie reagregada cincide cn la original: MaxAggDif ~ 0
// * No hay datos negativos: MinValue >=0
// * La curvatura de la serie diarizada es menor que la escalonada 
//     MaxStdCurvatureRelative <= 1
//Esto �ltimo no se consigue a veces cuando hay muchos ceros por lo que 
//requiere una supervisi�n manual por ahora

Real MinValue = 1/0;
Real MaxAggDif = -1/0;
Real MaxStdCurvatureRelative = -1/0;
Set checkInfo = EvalSet(checkSeries,Set(Set S)
{[[
  Text name = Name(S[1]);
  Text dating = DatingName(S[1]);
  Date first = First(S[1]);
  Date last = First(S[1]);
  Real length = CountS(S[1]);
  Real numZeroes = SumS(EQ(S[1],0));
  Real minValue = 
  {
    Real mn = MinS(S[4]);
    Real MinValue:=Min(MinValue,mn); 
    mn
  };
  Real maxAggDif = 
  { 
    Real mx=MaxS(Abs(S[1]-S[2])); 
    Real MaxAggDif:=(Max(MaxAggDif,mx)+1)-1; 
    mx 
  };
  Real stdCurvatureRelative = 
  {
    Real cr = 
      Sqrt(AvrS((((1-B)^2):S[4])^2)) / 
      Sqrt(AvrS((((1-B)^2):S[3])^2));
    Real MaxStdCurvatureRelative := Max(MaxStdCurvatureRelative,cr);
    cr
  }
]]});

WriteLn("MinValue = "<<MinValue);
WriteLn("MaxAggDif = "<<MaxAggDif);
WriteLn("MaxStdCurvatureRelative = "<<MaxStdCurvatureRelative);

Set checkInfo.sorted = Sort(checkInfo,Real(Set a, Set b)
{
  Compare(b::stdCurvatureRelative,a::stdCurvatureRelative)
})
/* * /
Real t0 := Copy(Time);
Set sM2 = EvalSet(subSample, Serie(Serie s)
{
  Serie acum = DifEq(B/(1-B), s, 0);
  Serie acumD = StdLib::Interpolate::Scalar::invCh(acum, $trg, "cspline", 1);
  Eval(Name(s)+"_cspline = (F-1):acumD")
});
Real time.cspline = (Copy(Time)-t0) / Card(dailySeries);
/* */

